# arviem-locator
This is the test project of a simple location management.

## start-up
After the repository clone launch npm install 

	npm install

When the installation process is terminated run

	ng serve 
	
for a dev server  on http://localhost:4200/.

## Tests
one e2e test is implemented:
It run the application, create a new location and, after dialog confirmation, verify that the location is the first record on
the resulting table view

Run: 
	
	ng e2e 
	
to launch protractor and the e2e test.

## Prerequisite
This project use Angular CLI 8.1.2 and Node 12.7.0.